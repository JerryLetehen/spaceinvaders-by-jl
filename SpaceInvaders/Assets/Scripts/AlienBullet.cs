﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienBullet : MonoBehaviour {

    private Rigidbody2D rigidBody;
    [SerializeField] private float speed = 30;
    [SerializeField] private Sprite explodedShipImage;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector2.down * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Wall")
        {
            Destroy(gameObject);
        }

        if(collision.tag == "Bullet")
        {
            Destroy(gameObject);
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.AlienBuzz);
        }

        if(collision.tag == "Player")
        {
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.ShipExplosion);
            collision.GetComponent<SpriteRenderer>().sprite = explodedShipImage;
            Destroy(gameObject);
            DestroyObject(collision.gameObject, 0.5f);
        }

        if(collision.tag == "Shield")
        {
            Destroy(gameObject);
            DestroyObject(collision.gameObject);
        }
    }

    private void OnBecomeVisible()
    {
        Destroy(gameObject);
    }
}
