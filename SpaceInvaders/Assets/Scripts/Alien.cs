﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien : MonoBehaviour {

    [SerializeField] private float speed = 10;
    [SerializeField] private Sprite startingImage;
    [SerializeField] private Sprite altImage;
    private SpriteRenderer spriteRenderer;
    //[SerializeField] private float secBeforeSpriteChange = 0.5f;
    [SerializeField] private GameObject alienBullet;
    [SerializeField] private float minFireRateTime = 1.0f;
    [SerializeField] private float maxFireRateTime = 3.0f;
    [SerializeField] private float fireWaitTime = 3.0f;
    [SerializeField] private Sprite explodedShipImage;
    private Rigidbody2D rigidBody;
    public Rigidbody2D RigidBody { get { return rigidBody; } }

    public bool IsDying { get; set; }

    private void Awake()
    {
        StartCoroutine(WaitForInstance());
    }

    private IEnumerator WaitForInstance()
    {
        while(!SoundManager.Instance)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector2.right * speed;
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(ChangeAlienSprite());
        StartCoroutine(WaitForShoot(fireWaitTime + Random.Range(minFireRateTime, maxFireRateTime)));
    }

    private void Turn(float direction)
    {
        rigidBody.velocity = new Vector2(direction, rigidBody.velocity.y) * speed;
    }

    private void MoveDown()
    {
        transform.position = new Vector2(transform.position.x, transform.position.y - 1f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "LeftWall")
        {
            Turn(1f);
            MoveDown();
        }

        if(collision.gameObject.name == "RightWall")
        {
            Turn(-1f);
            MoveDown();
        }
    }

    public IEnumerator ChangeAlienSprite()
    {
        while(true)
        {
            // if the alien is dying, he doesn't change his sprite
            if (!IsDying)
            {
                if (spriteRenderer.sprite == startingImage)
                {
                    spriteRenderer.sprite = altImage;
                }
                else
                {
                    spriteRenderer.sprite = startingImage;
                }
            }
            yield return new WaitForSeconds(Random.Range(minFireRateTime, maxFireRateTime));
        }
    }

    private IEnumerator AlienShoot()
    {
        while(true)
        {
            if(!IsDying)
            {
                Instantiate(alienBullet, transform.position, Quaternion.identity);
                SoundManager.Instance.PlayOneShot(SoundManager.Instance.AlienShot);
            }
            yield return new WaitForSeconds(Random.Range(minFireRateTime, maxFireRateTime));
        }
    }

    private IEnumerator WaitForShoot(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        StartCoroutine(AlienShoot());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.ShipExplosion);
            collision.GetComponent<SpriteRenderer>().sprite = explodedShipImage;
            Destroy(gameObject);
            DestroyObject(collision.gameObject, 0.5f);
        }
    }
}