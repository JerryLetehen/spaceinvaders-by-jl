﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    public static GameManager Instanse = null;

    private GameObject[] aliens = null;
    public int BulletsCount { get; set; }
    private GameObject player;

    private Camera cam;

    private bool isGameWin;
    private bool extraWin;

    [SerializeField] private float timeToStart = 1f;

    [SerializeField] private GameObject canvasNextLevel;
    [SerializeField] private GameObject canvasRestart;
    private Text bullets;

    private void Awake()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        StartCoroutine(WaitingForStart(timeToStart));
    }

    private void Start()
    {
        if (Instanse == null) { Instanse = this; }
        else if (Instanse != this) { Destroy(gameObject); }

        player = GameObject.FindGameObjectWithTag("Player");

        var textUI = GameObject.Find("Score").GetComponent<Text>();
        int score;
        if(SceneManager.GetActiveScene().name == "Extra")
        {
            score = GV.score = GV.scoreAtLast;
        } else {
            score = GV.score;
        }
        textUI.text = score.ToString();

        var textLevel = GameObject.Find("Level").GetComponent<Text>();
        if (SceneManager.GetActiveScene().name == "MainScene")
        {
            textLevel.text = "Level: 1";
        }
        else
        {
            textLevel.text = "Level: " + SceneManager.GetActiveScene().name;
        }
        var aliens = GameObject.FindGameObjectsWithTag("Alien");
        foreach(var alien in aliens)
        {
            BulletsCount += 2;
        }

        bullets = GameObject.Find("Bullets").GetComponent<Text>();
        bullets.text = "Bullets: " + BulletsCount;

    }

    private void FixedUpdate()
    {
        var bulletsObjects = GameObject.FindGameObjectsWithTag("Bullet");
        var bulletsText = GameObject.Find("Bullets").GetComponent<Text>();
        bulletsText.text = "Bullets: " + BulletsCount;
        aliens = GameObject.FindGameObjectsWithTag("Alien");

        if (aliens.Length == 0 && !isGameWin) { WinGame(); print("win"); }
        else
            if (BulletsCount == 0 && bulletsObjects.Length == 0 && aliens.Length > 1) { LoseGame(); print("lose"); }
        else
            if (BulletsCount == 0 && bulletsObjects.Length == 0 && aliens.Length == 1)
        {
            var lastAlien = GameObject.FindGameObjectWithTag("Alien").GetComponent<Alien>();
            if (lastAlien.IsDying) { WinGame(); print("win"); } else { LoseGame(); print("lose"); }
        }
        else
            if (player == null) { LoseGame(); print("lose"); }
#if UNITY_EDITOR 
            else { print(1); } 
#endif

    }

    public void IncreaseTextUIScore()
    {
        var textUI = GameObject.Find("Score").GetComponent<Text>();
        int score = int.Parse(textUI.text);
        score += 10;
        GV.score = score;
        textUI.text = score.ToString();
    }

    private IEnumerator WaitingForStart(float time)
    {
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(time);
        Time.timeScale = 1f;
    }

    private void WinGame()
    {
        cam.cullingMask = 1 << 4;
        GV.score += BulletsCount * 10;
        Time.timeScale = 0f;
        canvasNextLevel.SetActive(true);
        var textUI = GameObject.Find("Score").GetComponent<Text>();
        textUI.text = GV.score.ToString();
        if (SceneManager.GetActiveScene().name == "5") { LastLevelCanvas(); }
        else if (SceneManager.GetActiveScene().name == "Extra") { ExtraLevelCanvas(); }
    }

    public void RestartGame()
    {
        GV.score = 0;
        SceneManager.LoadScene("MainScene");
    }

    private void LoseGame()
    {
        cam.cullingMask = 1 << 4;
        Time.timeScale = 0f;
        canvasRestart.SetActive(true);
    }

    public void NextScene()
    {
        int sceneToLoad = -1;
        switch (SceneManager.GetActiveScene().name)
        {
            case "MainScene":
                sceneToLoad = 2;
                break;
            case "2":
                sceneToLoad = 3;
                break;
            case "3":
                sceneToLoad = 4;
                break;
            case "4":
                sceneToLoad = 5;
                break;
            default:
                Debug.Log("Error of get current scene");
                break;
        }
        SceneManager.LoadScene(sceneToLoad.ToString());
    }

    public void DecreaseCountOfBulets(int damage)
    {
        BulletsCount -= damage;
    }

    private void LastLevelCanvas()
    {
        var text = GameObject.Find("Next").GetComponent<Text>();
        var button = GameObject.Find("Go!").GetComponent<Button>();
        if(GV.score >= 1770){
            text.text = "Nice!!! You won last level! And your score is : " + GV.score + " | But I have something for ya! Extra Level! Can you win it???";
            button.enabled = true;
            button.interactable = true;

        } else {
            text.text = "Not bad... But you need to get more than 1770 score points to unlock Extra Level! Good luck!";
            button.enabled = false;
            button.interactable = false;
        }
        text.alignByGeometry = true;
        text.resizeTextForBestFit = true;
    }

    public void GoExtraLevel()
    {
        if(extraWin)
        {
            GV.score = GV.scoreAtLast;
        } else {
            GV.scoreAtLast = GV.score;
        }
        SceneManager.LoadScene("Extra");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void RestartExtra()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void ExtraLevelCanvas()
    {
        extraWin = true;
        var text = GameObject.Find("End").GetComponent<Text>();
        text.text = "Perfect!!! Score: " + GV.score + " Have Fun!!";
    }
}
