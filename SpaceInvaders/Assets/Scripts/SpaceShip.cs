﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour {

    [SerializeField] private float speed = 30;

    [SerializeField] private GameObject theBullet;

    private void FixedUpdate()
    {
        float x = Input.GetAxisRaw("Horizontal");
        GetComponent<Rigidbody2D>().velocity = new Vector2(x, 0f) * speed;
    }

    private void Update()
    {
        if(Input.GetButtonDown("Jump") && GameManager.Instanse.BulletsCount != 0)
        {
            Instantiate(theBullet, transform.position, Quaternion.identity);
            GameManager.Instanse.DecreaseCountOfBulets(1);
            SoundManager.Instance.PlayOneShot(SoundManager.Instance.BulletFire);
        }
    }
}
