﻿using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField] private float speed = 30;

    private Rigidbody2D rigidBody;

    [SerializeField] private Sprite explodedAlienImage;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector2.up * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Wall")
        {
            Destroy(gameObject);
        }

        if(collision.tag == "AlienBullet")
        {
            Destroy(gameObject);
        }

        if(collision.tag == "Alien")
        {
            if (collision.GetComponent<SpriteRenderer>().sprite != explodedAlienImage)
            {
                var alien = collision.gameObject.GetComponent<Alien>();
                alien.IsDying = true;
                alien.RigidBody.velocity = Vector2.zero;
                SoundManager.Instance.PlayOneShot(SoundManager.Instance.AlienDies);
                GameManager.Instanse.IncreaseTextUIScore();
                collision.GetComponent<SpriteRenderer>().sprite = explodedAlienImage;
                Destroy(gameObject);
                DestroyObject(collision.gameObject, 0.5f);
            }
        }

        if(collision.tag == "Shield")
        {
            Destroy(gameObject);
            DestroyObject(collision.gameObject);
        }
    }

    private void OnBecomeVisible()
    {
        print("OnBecomeVisible");
        Destroy(gameObject);
    }
}
