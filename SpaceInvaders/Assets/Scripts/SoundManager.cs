﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance = null;

    [SerializeField] private AudioClip alienBuzz;
    public AudioClip AlienBuzz { get { return alienBuzz; } }
    [SerializeField] private AudioClip alienDies;
    public AudioClip AlienDies { get { return alienDies; } }
    [SerializeField] private AudioClip alieanShot;
    public AudioClip AlienShot { get { return alieanShot; } }
    [SerializeField] private AudioClip bulletFire;
    public AudioClip BulletFire { get { return bulletFire; } }
    [SerializeField] private AudioClip shipExplosion;
    public AudioClip ShipExplosion { get { return shipExplosion; } }

    private AudioSource soundEffectAudio;

    private void Start()
    {
        if (Instance == null) { Instance = this; }
        else if (Instance != this) { Destroy(gameObject); }

        soundEffectAudio = GetComponent<AudioSource>();
    }

    public void PlayOneShot(AudioClip clip)
    {
        soundEffectAudio.PlayOneShot(clip);
    }
}
